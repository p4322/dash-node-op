//@ts-check

const { expect } = require("chai");

const MID = require("../src/mid/7410.js");

describe("MID 7410", () => {
  it("parser rev 1 with values", (done) => {
    // {
    //     const hexBuffer = Buffer.from("4A00", "hex");
    //     const decimalNumber = hexBuffer.readUIntBE(0, 2);
    //     const decimalNumber2 = hexBuffer.readUInt16BE();

    //     console.log("Hex Buffer:", hexBuffer.toString("hex"));
    //     console.log("Decimal Number:", decimalNumber);
    //     console.log("Decimal Number2:", decimalNumber2);
    // }


    // const replaceItemsFrom16BETo8 = MID.replaceItemsFrom16BETo8;
    
    // let curveDataBuffer = Buffer.from("4AFFFE5D1AFFFE4A", "hex");

    // console.log("curveDataBuffer:", curveDataBuffer.toString("hex"));

    // curveDataBuffer = replaceItemsFrom16BETo8(curveDataBuffer, 0xfffe, 0x00);
    // curveDataBuffer = replaceItemsFrom16BETo8(curveDataBuffer, 0xffff, 0xff);

    // console.log("curveDataBuffer:", curveDataBuffer.toString("hex"));

    // throw new Error("stop");
    // return;

    // console.log("length -> ", Buffer.byteLength("4A005D1A0000", "hex"));
    // console.log(
    //   "length -> ",
    //   Buffer.byteLength("4A005D1A00004A005D1A0000", "hex")
    // );
    // console.log(
    //   "length -> ",
    //   Buffer.byteLength("4A005D1A00004A005D1A0000", "hex")
    // );
    // console.log("length -> ", Buffer.byteLength("4A005D1A00004A005D1A00005B00", "hex"));

    let msg = {
      mid: 7410,
      revision: 1,
      payload: Buffer.concat([
        Buffer.from(
          "01010200103123456789012340412345678901234051234567890123406123407020801"
        ),
        Buffer.from("4BFFFE5E1BFFFEFFFE4BFFFE5E1BFFFEFFFE", "hex"), //Buffer.from("4A005D1A00004A005D1A0000", "hex"),
        Buffer.from("\u000011"),
      ]),
    };

    // const inputBuffer = Buffer.from("4A005D1A00004A005D1A00005B00", "hex");
    // const swappedBuffer = swapTorqueAndAngleBytes(inputBuffer);
    // console.log("Before: ", inputBuffer.toString("hex").toUpperCase());
    // console.log("After : ", swappedBuffer.toString("hex").toUpperCase());
    // return;

    MID.parser(msg, {}, (err, data) => {
      if (err) {
        console.log(err);
      }

      console.log(data);

      expect(data).to.be.deep.equal({
        mid: 7410,
        revision: 1,
        payload: {
          toolNumber: 1,
          psetNumber: 1,
          timeCoefficient: 12345678901234,
          torqueCoefficient: 12345678901234,
          angleCoefficient: 12345678901234,
          measurementPoints: 1234,
          telegramsCount: 2,
          telegramId: 1,
          curveData: [
            {
              angle: 6749,
              torque: 74,
            },
            {
              angle: 6749,
              torque: 74,
            },
          ],
        },
      });

      done();
    });
  });
});
