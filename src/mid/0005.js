//@ts-check


/**
 * @name MID0005
 * @class
 * @param {object} MID0005 
 * @param {number} MID0005.midNumber
 */

const helpers = require("../helpers.js");
const processParser = helpers.processParser;
const serializerField = helpers.serializerField;

function parser(msg, opts, cb) {

    let buffer = msg.payload;
    msg.payload = {};

    let position = {
        value: 0
    };

    msg.revision = msg.revision || 1;

    if (msg.revision <= 0 || msg.revision > 999) {
        console.log(
            `[Parser MID${msg.mid}] invalid revision [${msg.revision}]`
        );
        cb(
            new Error(
              `[Parser MID${msg.mid}] invalid revision [${msg.revision}]`
            )
        );
        return;
    }

    processParser(msg, buffer, "midNumber", "number", 4, position, cb) &&
        cb(null, msg);

    // switch (msg.revision) {
    //     case 1:
    //         console.log("RECEIVING MID 0005 REVISION 1")
    //         processParser(msg, buffer, "midNumber", "number", 4, position, cb) &&
    //             cb(null, msg);
    //         break;

    //     default:
    //         cb(new Error(`[Parser MID${msg.mid}] invalid revision [${msg.revision}]`));
    //         break;
    // }
}

function serializer(msg, opts, cb) {

    let buf;
    let statusprocess = false;

    let position = {
        value: 0
    };

    msg.revision = msg.revision || 1;

    switch (msg.revision) {
        case 1:
            buf = Buffer.alloc(4);

            position.value = 4;

            statusprocess = serializerField(msg, buf, "midNumber", "number", 4, position, cb);

            if (!statusprocess) {
                return;
            }

            msg.payload = buf;

            cb(null, msg);

            break;

        default:
            cb(new Error(`[Serializer MID${msg.mid}] invalid revision [${msg.revision}]`));
            break;
    }
}

function revision() {
    return [1];
}

module.exports = {
    parser,
    serializer,
    revision
};