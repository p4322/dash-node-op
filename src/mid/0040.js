//@ts-check


function parser(msg, opts, cb) {
    let buffer = msg.payload;
    msg.payload = buffer.toString("ascii");
    cb(null, msg);
}

function serializer(msg, opts, cb) {
    let buf = Buffer.from("");
    msg.payload = buf;
    cb(null, msg);
}

function revision() {
    return [1, 2, 3, 4, 5];
}

module.exports = {
    parser,
    serializer,
    revision
};