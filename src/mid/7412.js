//@ts-check

function parser(msg, opts, cb) {
  let buffer = msg.payload;
  msg.payload = buffer.toString("ascii");
  cb(null, msg);
}

function serializer(msg, opts, cb) {
  let buf = Buffer.from(msg.payload);
  msg.payload = buf;
  cb(null, msg);
}

function revision() {
  return [1];
}

module.exports = {
  parser,
  serializer,
  revision,
};
