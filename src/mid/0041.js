//@ts-check

const helpers = require("../helpers.js");
const processParser = helpers.processParser;
const processKey = helpers.processKey;
const constantsMID = require("./MidConstants/MID0041.json");

function parser(msg, opts, cb) {
  let buffer = msg.payload;

  msg.payload = {};

  let status = true;

  var position = { value: -1 };

  switch (msg.revision) {
    case 5:
      position = { value: 164 };

      status =
        status &&
        processKey(msg, buffer, "toolModel", 16, 2, position, cb) &&
        processParser(msg, buffer, "toolModel", "string", 12, position, cb);

    case 4:
      position = { value: 160 };

      status =
        status &&
        processKey(msg, buffer, "primaryToolType", 15, 2, position, cb) &&
        processParser(
          msg,
          buffer,
          "primaryToolType",
          "string",
          2,
          position,
          cb
        );

      msg.payload["primaryToolType"] =
        constantsMID.primaryTool[msg.payload["primaryToolType"]] ??
        msg.payload["primaryToolType"];
    case 3:
      position = { value: 136 };

      status =
        status &&
        processKey(msg, buffer, "toolMaxTorque", 12, 2, position, cb) &&
        processParser(
          msg,
          buffer,
          "toolMaxTorque",
          "number",
          6,
          position,
          cb
        ) &&
        processKey(msg, buffer, "gearRatio", 13, 2, position, cb) &&
        processParser(msg, buffer, "gearRatio", "number", 6, position, cb) &&
        processKey(msg, buffer, "toolFullSpeed", 14, 2, position, cb) &&
        processParser(msg, buffer, "toolFullSpeed", "number", 6, position, cb);

      try {
        msg.payload["toolMaxTorque"] = msg.payload["toolMaxTorque"] / 100;
      } catch (e) {
        /* ignore */
      }
      try {
        msg.payload["gearRatio"] = msg.payload["gearRatio"] / 100;
      } catch (e) {
        /* ignore */
      }
      try {
        msg.payload["toolFullSpeed"] = msg.payload["toolFullSpeed"] / 100;
      } catch (e) {
        /* ignore */
      }

    case 2:
      position = { value: 61 };

      status =
        status &&
        processKey(msg, buffer, "calibrationValue", 5, 2, position, cb) &&
        processParser(
          msg,
          buffer,
          "calibrationValue",
          "number",
          6,
          position,
          cb
        ) &&
        processKey(msg, buffer, "lastServiceDate", 6, 2, position, cb) &&
        processParser(
          msg,
          buffer,
          "lastServiceDate",
          "string",
          19,
          position,
          cb
        ) &&
        processKey(
          msg,
          buffer,
          "tighteningsSinceService",
          7,
          2,
          position,
          cb
        ) &&
        processParser(
          msg,
          buffer,
          "tighteningsSinceService",
          "number",
          10,
          position,
          cb
        ) &&
        processKey(msg, buffer, "toolType", 8, 2, position, cb) &&
        processParser(msg, buffer, "toolType", "number", 2, position, cb) &&
        processKey(msg, buffer, "motorSize", 9, 2, position, cb) &&
        processParser(msg, buffer, "motorSize", "number", 2, position, cb) &&
        processKey(msg, buffer, "_openEndData", 10, 2, position, cb) &&
        processParser(msg, buffer, "_openEndData", "string", 3, position, cb) &&
        processKey(
          msg,
          buffer,
          "controllerSoftwareVersion",
          11,
          2,
          position,
          cb
        ) &&
        processParser(
          msg,
          buffer,
          "controllerSoftwareVersion",
          "string",
          19,
          position,
          cb
        );

      msg.payload["toolType"] =
        constantsMID.toolType[msg.payload["toolType"]] ??
        msg.payload["toolType"];
      msg.payload["motorSize"] =
        constantsMID.motorSize[msg.payload["motorSize"]] ??
        msg.payload["motorSize"];
      msg.payload["motorRotation"] =
        constantsMID.motorRotation[msg.payload["_openEndData"]?.[2]];
      msg.payload["tighteningDirection"] =
        constantsMID.tighteningDirection[msg.payload["_openEndData"]?.[1]];
      msg.payload["useOpenEnd"] =
        constantsMID.useOpenEnd[msg.payload["_openEndData"]?.[0]];

      try {
        msg.payload["calibrationValue"] = msg.payload["calibrationValue"] / 100;
      } catch (e) {
        /* ignore */
      }

      delete msg.payload["_openEndData"];

    case 1:
      position = { value: 0 };

      status =
        status &&
        processKey(msg, buffer, "toolSerialNumber", 1, 2, position, cb) &&
        processParser(
          msg,
          buffer,
          "toolSerialNumber",
          "string",
          14,
          position,
          cb
        ) &&
        processKey(msg, buffer, "toolNumberOfTightening", 2, 2, position, cb) &&
        processParser(
          msg,
          buffer,
          "toolNumberOfTightening",
          "number",
          10,
          position,
          cb
        ) &&
        processKey(msg, buffer, "lastCalibrationDate", 3, 2, position, cb) &&
        processParser(
          msg,
          buffer,
          "lastCalibrationDate",
          "string",
          19,
          position,
          cb
        ) &&
        processKey(msg, buffer, "controllerSerialNumber", 4, 2, position, cb) &&
        processParser(
          msg,
          buffer,
          "controllerSerialNumber",
          "string",
          10,
          position,
          cb
        );
  }

  cb(null, msg);
}

function serializer(msg, opts, cb) {
  let buf = Buffer.from("");
  msg.payload = buf;
  cb(null, msg);
}

function revision() {
  return [5, 4, 3, 2, 1];
}

module.exports = {
  parser,
  serializer,
  revision,
};
