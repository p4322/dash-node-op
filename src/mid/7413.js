//@ts-check

const { testNul } = require("../helpers.js");
const helpers = require("../helpers.js");
const processKey = helpers.processKey;
const processParser = helpers.processParser;

function parser(msg, opts, cb) {
  let buffer = msg.payload;
  msg.payload = {};

  var position = { value: 0 };

  switch (msg.revision) {
    case 1:
      processParser(msg, buffer, "toolNumber", "number", 2, position, cb) &&
        processParser(msg, buffer, "psetNumber", "number", 3, position, cb) &&
        processParser(msg, buffer, "stepNumber", "string", 2, position, cb) &&
        processParser(msg, buffer, "stepType", "number", 2, position, cb) &&
        processParser(msg, buffer, "strategyType", "number", 2, position, cb) &&
        processParser(msg, buffer, "minTorque", "number", 6, position, cb) &&
        processParser(msg, buffer, "maxTorque", "number", 6, position, cb) &&
        processParser(msg, buffer, "abortTorque", "number", 6, position, cb) &&
        processParser(
          msg,
          buffer,
          "angelThreshold",
          "number",
          6,
          position,
          cb
        ) &&
        processParser(msg, buffer, "targetTorque", "number", 6, position, cb) &&
        processParser(msg, buffer, "reserved", "number", 6, position, cb) &&
        processParser(msg, buffer, "minAngle", "number", 6, position, cb) &&
        processParser(msg, buffer, "maxAngle", "number", 6, position, cb) &&
        processParser(msg, buffer, "abortAngle", "number", 6, position, cb) &&
        processParser(msg, buffer, "targetAngle", "number", 6, position, cb) &&
        processParser(msg, buffer, "reserved", "number", 6, position, cb) &&
        processParser(msg, buffer, "reserved", "number", 6, position, cb) &&
        processParser(msg, buffer, "reserved", "number", 6, position, cb) &&
        processParser(
          msg,
          buffer,
          "postStepDelay",
          "number",
          6,
          position,
          cb
        ) &&
        processParser(msg, buffer, "reserved", "number", 6, position, cb) &&
        processParser(
          msg,
          buffer,
          "downshiftSpeed",
          "number",
          6,
          position,
          cb
        ) &&
        processParser(msg, buffer, "reserved", "number", 6, position, cb) &&
        processParser(msg, buffer, "reserved", "number", 6, position, cb) &&
        processParser(msg, buffer, "reserved", "number", 2, position, cb) &&
        processParser(msg, buffer, "reserved", "number", 6, position, cb) &&
        processParser(msg, buffer, "reserved", "number", 6, position, cb) &&
        processParser(msg, buffer, "curveIndex1", "number", 4, position, cb) &&
        processParser(msg, buffer, "curveIndex2", "number", 4, position, cb) &&
        processParser(msg, buffer, "reserved", "number", 6, position, cb) &&
        processParser(msg, buffer, "reserved", "number", 6, position, cb) &&
        processParser(msg, buffer, "reserved", "number", 6, position, cb) &&
        processParser(
          msg,
          buffer,
          "rotationDirection",
          "number",
          2,
          position,
          cb
        ) &&
        processParser(msg, buffer, "rotateType", "number", 1, position, cb) &&
        processParser(msg, buffer, "rotationTime", "number", 3, position, cb) &&
        processParser(
          msg,
          buffer,
          "numberOfRotations",
          "number",
          1,
          position,
          cb
        ) &&
        processParser(msg, buffer, "torqueUnit", "number", 4, position, cb) &&
        processParser(
          msg,
          buffer,
          "psetLastModificationDate",
          "number",
          19,
          position,
          cb
        ) &&
        testNul(msg, buffer, "char nul", position, cb);

      cb(null, msg);

      break;
  }
}

function serializer(msg, opts, cb) {
  throw new Error("7413 serializer not implemented");
}

module.exports = {
  parser,
  serializer,
};

// /**
//  * Removes occurrences of a specific value from a Buffer.
//  *
//  * @param {Buffer} curveDataOriginal - The original Buffer to remove values from.
//  * @param {number} targetValue - The value to be removed from the Buffer.
//  * @param {number} replacementValue - The value to be removed from the Buffer.
//  * @returns {Buffer} - A new Buffer with the specified values removed.
//  */
// function replaceItemsFrom16LETo8(curveDataOriginal, targetValue, replacementValue) {
//         const newBuffer = Buffer.alloc(curveDataOriginal.length); // Create a new Buffer with the same length

//         let newIndex = 0;

//         for (let i = 0; i < curveDataOriginal.length - 4; i += 2) {
//             const value = curveDataOriginal.readUInt16LE(i); //readUInt16BE for big endian

//             if (value !== targetValue) {
//                 // If the value is not equal to the target value, copy it to the new buffer
//                 newBuffer.writeUInt16LE(value, newIndex);
//                 newIndex += 2;
//             }
//             else {
//                 newBuffer.writeUInt8(replacementValue, newIndex);
//                 newIndex += 1;
//             }
//         }

//         // The newBuffer now contains the values from yourBuffer excluding the targetValue occurrences
//         return newBuffer.subarray(0, newIndex);
// }
