//@ts-check

/*
    MID 7410
    [01xx]          [02xxx]       [03xxx..xxx]        [04xxx..xxx]       [05xxx..xxx]       [06xxxx]              [07xx]            [08xx]
    (4) 0-4         (5) 5-9       (16) 10-25          (16) 26-41         (16) 42-57         (6) 58-63             (4) 64-67         (4) 68-71
    toolNumber      psetNumber    timeCoefficient     torqueCoefficient  angleCoefficient   measurementPoints     telegramsCount    telegramId

    Data fields
    [xxx...xxx]      
    (n) 72-n        
    curveData    

*/
/*
    payload: {
        toolNumber: {number}
        psetNumber: {number}
        timeCoefficient: {number}
        torqueCoefficient: {number}
        angleCoefficient: {number}
        transducerType: {number}
        measurementPoints: {number}
        telegramsCount: {number}
        telegramId: {object}
        curveData: {object[]}
    }
*/

const helpers = require("../helpers.js");
const processKey = helpers.processKey;
const processParser = helpers.processParser;

function parser(msg, opts, cb) {
  let buffer = msg.payload;
  msg.payload = {};

  var position = { value: 0 };

  // {
  //   const fs = require("fs");
  //   // Get current date and time
  //   const currentDate = new Date();
  //   const formattedDate = currentDate
  //     .toISOString()
  //     .replace(/[-T:]/g, "")
  //     .split(".")[0];

  //   // Create a file name with the current date and time including milliseconds
  //   const fileName = `data_${formattedDate}.txt`;

  //   // Write the buffer to the file synchronously
  //   try {
  //     console.log("Writing buffer to file " + fileName + "...");
  //     fs.writeFileSync(fileName, buffer);
  //     console.log(`Buffer successfully saved to ${fileName}`);
  //   } catch (err) {
  //     console.error("Error writing to file:", err);
  //   }
  // }

  switch (msg.revision) {
    case 1:
      processKey(msg, buffer, "toolNumber", 1, 2, position, cb) &&
        processParser(msg, buffer, "toolNumber", "number", 2, position, cb) &&
        processKey(msg, buffer, "psetNumber", 2, 2, position, cb) &&
        processParser(msg, buffer, "psetNumber", "number", 3, position, cb) &&
        processKey(msg, buffer, "timeCoefficient", 3, 2, position, cb) &&
        processParser(
          msg,
          buffer,
          "timeCoefficient",
          "number",
          14,
          position,
          cb
        ) &&
        processKey(msg, buffer, "torqueCoefficient", 4, 2, position, cb) &&
        processParser(
          msg,
          buffer,
          "torqueCoefficient",
          "number",
          14,
          position,
          cb
        ) &&
        processKey(msg, buffer, "angleCoefficient", 5, 2, position, cb) &&
        processParser(
          msg,
          buffer,
          "angleCoefficient",
          "number",
          14,
          position,
          cb
        ) &&
        processKey(msg, buffer, "measurementPoints", 6, 2, position, cb) &&
        processParser(
          msg,
          buffer,
          "measurementPoints",
          "number",
          4,
          position,
          cb
        ) &&
        processKey(msg, buffer, "telegramsCount", 7, 2, position, cb) &&
        processParser(
          msg,
          buffer,
          "telegramsCount",
          "number",
          2,
          position,
          cb
        ) &&
        processKey(msg, buffer, "telegramId", 8, 2, position, cb) &&
        processParser(msg, buffer, "telegramId", "number", 2, position, cb);
      //&&
      //testNul(msg, buffer, "char nul", position, cb);
      // processParser(msg, buffer, "traceSample", "string", 2, position, cb)

      /** @type Buffer */
      const curveDataOriginalBuffer = buffer.slice(
        position.value,
        buffer.length
      );

      if (curveDataOriginalBuffer.length === 0) {
        cb(null, msg);
        break;
      }

      let curveDataBuffer = curveDataOriginalBuffer;

      curveDataBuffer = replaceItemsFrom16BETo8(curveDataBuffer, 0xfffe, 0x00);
      curveDataBuffer = replaceItemsFrom16BETo8(curveDataBuffer, 0xffff, 0xff);
      curveDataBuffer = subtractOneFromBytesGreaterThanZero(curveDataBuffer);
      curveDataBuffer = swapTorqueAndAngleBytes(curveDataBuffer);

      // console.log(curveDataBuffer.toString("hex").toUpperCase());

      const torqueSize = 2; // 2 bytes for torque
      const angleSize = 4; // 4 bytes for angle

      const curveData = [];

      for (
        let i = 0;
        i < curveDataBuffer.length - 1;
        i += torqueSize + angleSize
      ) {
        const torqueSlice = curveDataBuffer.subarray(i, i + torqueSize);
        const angleSlice = curveDataBuffer.subarray(
          i + torqueSize,
          i + torqueSize + angleSize
        );
        if (
          torqueSlice.byteLength === torqueSize &&
          angleSlice.byteLength === angleSize
        ) {
          curveData.push([
            torqueSlice.readUInt16BE(),
            angleSlice.readUInt32BE(),
          ]);
        }
      }

      msg.payload.curveData = curveData;

      // console.log(JSON.stringify(msg.payload.curveData, null, 2));

      // const step = 2;
      // msg.payload.traceSample = [];
      // for (let i = position.value; i < buffer.length - step; i += step) {
      //   const value = buffer.readIntBE(i, step);
      //   msg.payload.traceSample.push(value);
      // }

      // msg.payload._traceType = msg.payload.traceType;
      // msg.payload.traceType = constantsMID.traceType[msg.payload.traceType];

      // msg.payload.unit = msg.payload.unit;
      // msg.payload.unitName = constantsMID.unit[msg.payload.unit];

      // msg.payload._transducerType = msg.payload.transducerType;
      // msg.payload.transducerType = `transducer ${msg.payload.transducerType}`;

      // const coefficientFieldData = msg.payload.fieldData?.find((item) =>
      //   ["02213", "02214"].includes(item.parameterID)
      // );
      // const dataValue = Number(coefficientFieldData?.dataValue);

      // if (isNaN(dataValue)) {
      //   console.log(
      //     "******* fieldData is missing or undefined ******",
      //     msg.payload.fieldData
      //   );
      // }

      // if (!isNaN(dataValue) && dataValue !== 1) {
      //   switch (coefficientFieldData?.parameterID) {
      //     case "02213":
      //       //Physical value = Binary value / Coefficient
      //       msg.payload.traceSample = msg.payload.traceSample.map(
      //         (binaryValue) => Number((binaryValue / dataValue).toFixed(2))
      //       );
      //       break;
      //     case "02214":
      //       //Physical value = Binary value * Coefficient
      //       msg.payload.traceSample = msg.payload.traceSample.map(
      //         (binaryValue) => Number((binaryValue * dataValue).toFixed(2))
      //       );
      //       break;
      //     default:
      //       break;
      //   }
      // }

      cb(null, msg);

      break;
  }
}

function serializer(msg, opts, cb) {
  throw new Error("7408 serializer not implemented");

  // let info = msg.payload;
  // let buf;

  // switch(msg.revision){

  //     case 1:

  //         //Calc alloc
  //         //Message Base (50)
  //         //fieldPID
  //         //fieldData (17 + length) x numberPID
  //         //resolutionFields (18 + length) x numberResolution

  //         let size = 50;

  //         if(info.dataFields.length > 0){
  //             for(let x = 0; x < info.dataFields.length; x++){
  //                 size += info.dataFields[x].length;
  //             }
  //         }
  //         size += (17 * info.dataFields.length);

  //         if(info.resolutionFields.length > 0){
  //             for(let x = 0; x < info.resolutionFields.length; x++){
  //                 size += info.resolutionFields[x].length;
  //             }
  //         }
  //         size += (18 * info.resolutionFields.length);

  //         buf = Buffer.alloc(size);

  //         let pos = 0;

  //         buf.write(padLeft(info.resultID, 10), pos, "ascii");
  //         pos += 10;

  //         buf.write(padRight(info.timeStamp, 19, 10, " "), pos, "ascii");
  //         pos += 19;

  //         buf.write(padLeft(info.numberPID, 3), pos, "ascii");
  //         pos += 3;

  //         //info.dataFields
  //         if(info.numberPID > 0){
  //             for(let x = 0; x < info.numberPID; x++){

  //                 buf.write(padLeft(info.dataFields[x].parameterID, 5), pos, "ascii");
  //                 pos += 5;

  //                 buf.write(padLeft(info.dataFields[x].length, 3), pos, "ascii");
  //                 pos += 3;

  //                 buf.write(padLeft(info.dataFields[x].dataType, 2), pos, "ascii");
  //                 pos += 2;

  //                 buf.write(padLeft(info.dataFields[x].unit, 3), pos, "ascii");
  //                 pos += 3;

  //                 buf.write(padLeft(info.dataFields[x].stepNumber, 4), pos, "ascii");
  //                 pos += 4;

  //                 buf.write(padRight(info.dataValue, info.dataFields[x].length, 10, " "), pos, "ascii");
  //                 pos += info.dataFields[x].length;

  //             }
  //         }

  //         buf.write(padLeft(info.traceType, 2), pos, "ascii");
  //         pos += 2;

  //         buf.write(padLeft(info.transducerType, 2), pos, "ascii");
  //         pos += 2;

  //         buf.write(padLeft(info.unit, 3), pos, "ascii");
  //         pos += 3;

  //         buf.write(padLeft(info.numberResolution, 3), pos, "ascii");
  //         pos += 3;

  //     break;

  //     }

  // msg.payload = buf;

  // cb(null, msg);
}

module.exports = {
  parser,
  serializer,
};

// /**
//  * Removes occurrences of a specific value from a Buffer.
//  *
//  * @param {Buffer} curveDataOriginal - The original Buffer to remove values from.
//  * @param {number} targetValue - The value to be removed from the Buffer.
//  * @param {number} replacementValue - The value to be removed from the Buffer.
//  * @returns {Buffer} - A new Buffer with the specified values removed.
//  */
// function replaceItemsFrom16LETo8(curveDataOriginal, targetValue, replacementValue) {
//         const newBuffer = Buffer.alloc(curveDataOriginal.length); // Create a new Buffer with the same length

//         let newIndex = 0;

//         for (let i = 0; i < curveDataOriginal.length - 4; i += 2) {
//             const value = curveDataOriginal.readUInt16LE(i); //readUInt16BE for big endian

//             if (value !== targetValue) {
//                 // If the value is not equal to the target value, copy it to the new buffer
//                 newBuffer.writeUInt16LE(value, newIndex);
//                 newIndex += 2;
//             }
//             else {
//                 newBuffer.writeUInt8(replacementValue, newIndex);
//                 newIndex += 1;
//             }
//         }

//         // The newBuffer now contains the values from yourBuffer excluding the targetValue occurrences
//         return newBuffer.subarray(0, newIndex);
// }

/**
 * Removes occurrences of a specific value from a Buffer.
 *
 * @param {Buffer} curveDataOriginal - The original Buffer to remove values from.
 * @param {number} targetValue - The value to be removed from the Buffer.
 * @param {number} replacementValue - The value to be removed from the Buffer.
 * @returns {Buffer} - A new Buffer with the specified values removed.
 */
function replaceItemsFrom16BETo8(
  curveDataOriginal,
  targetValue,
  replacementValue
) {
  const newBuffer = Buffer.alloc(curveDataOriginal.length); // Create a new Buffer with the same length

  let newIndex = 0;

  for (let i = 0; i < curveDataOriginal.length; i++) {
    const byteValue = curveDataOriginal.readUInt8(i);

    let valueToCompare;
    if (i < curveDataOriginal.length - 1) {
      valueToCompare = curveDataOriginal.readUInt16BE(i);
    }

    if (valueToCompare === targetValue) {
      newBuffer.writeUInt8(replacementValue, newIndex);
      i += 1;
    } else {
      newBuffer.writeUInt8(byteValue, newIndex);
    }

    newIndex += 1;
  }

  return newBuffer.subarray(0, newIndex);
}

/**
 * Subtract 1 from each byte value greater than 0x00 in a Buffer.
 *
 * @param {Buffer} originalBuffer - The original Buffer to modify.
 * @returns {Buffer} - A new Buffer with 1 subtracted from each byte value greater than 0x00.
 */
function subtractOneFromBytesGreaterThanZero(originalBuffer) {
  // Create a new Buffer with the same length as the original
  let newBuffer = Buffer.alloc(originalBuffer.length);

  // Iterate through the original buffer
  for (let i = 0; i < originalBuffer.length - 1; i++) {
    const byteValue = originalBuffer.readUInt8(i);

    // Subtract 1 from each byte value greater than 0x00
    const newValue = byteValue > 0x00 ? byteValue - 1 : byteValue;

    // Write the new value to the new buffer
    newBuffer.writeUInt8(newValue, i);
  }

  return newBuffer;
}

/**
 * Swap torque and angle bytes in a given Buffer.
 *
 * @param {Buffer} buffer - Input Buffer containing torque and angle values.
 * @returns {Buffer} - Buffer with swapped torque and angle bytes.
 */
function swapTorqueAndAngleBytes(buffer) {
  const torqueSize = 2; // 2 bytes for torque
  const angleSize = 4; // 4 bytes for angle

  // Create a new buffer for the swapped values
  const swappedBufferArray = [];
  // Iterate over the buffer and swap torque and angle bytes
  for (let i = 0; i < buffer.length - 1; i += torqueSize + angleSize) {
    // Swap torque bytes
    const torqueSlice = buffer.subarray(i, i + torqueSize);
    const reversedTorque = Buffer.from([...torqueSlice].reverse());
    swappedBufferArray.push(reversedTorque);

    // Swap angle bytes
    const angleSlice = buffer.subarray(
      i + torqueSize,
      i + torqueSize + angleSize
    );
    const reversedAngle = Buffer.from([...angleSlice].reverse());
    swappedBufferArray.push(reversedAngle);
  }

  return Buffer.concat(swappedBufferArray);
}
